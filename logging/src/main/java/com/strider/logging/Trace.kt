package com.strider.logging

import com.github.ajalt.timberkt.Timber
import com.github.ajalt.timberkt.v
import com.strider.logging.Trace.Attribute
import com.strider.logging.Trace.Metric
import kotlin.reflect.KProperty

/**
 * Defines different traces captured and sent to Firebase.
 *
 * To define a new trace, create a subclass of [Trace] with a delegated property for each [Attribute] and [Metric] you wish to capture with the trace.
 * A name for the trace must also be defined.
 * The final step is to add a branch to the `when` statement in `getTraceForType`, mapping the type to a concrete instance of your new trace.
 */
sealed class Trace(private val traceInfo: TraceInfo) {
    abstract fun name(): String

    init {
        traceInfo.name = name()
    }

    ///////////////// TRACES \\\\\\\\\\\\\\\\\\\\\\

    class ReadEventSummariesFromDbTrace(traceInfo: TraceInfo) : Trace(traceInfo) {
        override fun name() = "Read event summaries from database"
        var itemCount: Long by Metric("items", traceInfo)
    }

    class DisplayTodoItemsTrace(traceInfo: TraceInfo) : Trace(traceInfo) {
        override fun name() = "Display Todo tab with items"
        var itemCount: Long by Metric("items", traceInfo)
        var lineItemCount: Long by Metric("lineItems", traceInfo)
    }

    class GroupAndSortTodoItemsTrace(traceInfo: TraceInfo) : Trace(traceInfo) {
        override fun name() = "Group & sort items on Todo tab"
        var itemCount: Long by Metric("items", traceInfo)
    }

    class CompleteWaveTrace(traceInfo: TraceInfo) : Trace(traceInfo) {
        override fun name() = "Complete wave"
        var pickedItemCount: Long by Metric("itemsPicked", traceInfo)
        var notPickedItemCount: Long by Metric("itemsNotPicked", traceInfo)
        var successful: Boolean? by Flag("wasCompletedSuccessfully", traceInfo)
    }

    //////////////////////////////

    /**
     * Track whether the trace has been started in order to track start-stop-start.
     * We don't want to indicate that a trace is running if it is started after having been stopped (can't restart a trace).
     */
    private var hasBeenStarted = false

    /**
     * Starts the trace. Traces created using `runTrace` are automatically started.
     */
    fun start() = traceInfo.start().also {
        if (!hasBeenStarted) {
            hasBeenStarted = true
            isRunning = true
        }
    }

    /**
     * Stops the trace. Traces created using `runTrace` are automatically stopped.
     */
    fun stop() = traceInfo.stop().also { isRunning = false }

    var isRunning: Boolean = false
        private set

    /**
     * An attribute of a trace, which has a string value.
     */
    data class Attribute(val name: String, private val simpleTrace: TraceInfo) {
        operator fun getValue(thisRef: Any?, property: KProperty<*>): String =
            simpleTrace.attributes[name] ?: ""

        operator fun setValue(thisRef: Any?, property: KProperty<*>, value: String) {
            Timber.tag(ANALYTICS_TAG).v { "Setting attribute $name to $value" }
            simpleTrace.attributes[name] = value
        }
    }

    /**
     * An attribute of a trace, which has a boolean value.
     */
    data class Flag(val name: String, private val simpleTrace: TraceInfo) {
        operator fun getValue(thisRef: Any?, property: KProperty<*>): Boolean? =
            simpleTrace.attributes[name]?.toBoolean()

        operator fun setValue(thisRef: Any?, property: KProperty<*>, value: Boolean?) {
            Timber.tag(ANALYTICS_TAG).v { "Setting attribute $name to $value" }
            simpleTrace.attributes[name] = value.toString()
        }
    }

    /**
     * A metric of a trace, which has a [Long] value.
     */
    data class Metric(val name: String, private val simpleTrace: TraceInfo) {
        operator fun getValue(thisRef: Any?, property: KProperty<*>): Long = simpleTrace.metrics[name] ?: 0

        operator fun setValue(thisRef: Any?, property: KProperty<*>, value: Long) {
            Timber.tag(ANALYTICS_TAG).v { "Setting metric $name to $value" }
            simpleTrace.metrics[name] = value
        }
    }
}