package com.strider.logging

import com.github.ajalt.timberkt.Timber
import java.util.*

class Tracer() {
    /**
     * Runs a trace on the code executed in the given block, returning some result.
     * NOTE: There is no need to call `start()` or `stop()`. These are handled automatically.
     */
    inline fun <reified T : Trace, R> runTraceWithResult(builder: TraceBuilder<T>, block: (trace: T) -> R): R {
        val trace = createTrace(builder)

        trace.start()

        return try {
            block(trace)
        } finally {
            trace.stop()
        }
    }

    /**
     * Runs a trace on the code executed in the given block.
     * NOTE: There is no need to call `start()` or `stop()`. These are handled automatically.
     */
    inline fun <reified T : Trace> runTrace(builder: TraceBuilder<T>, block: (trace: T) -> Unit) =
        runTraceWithResult(builder, block)

    /**
     * Creates a new [Trace] object that is not started.
     *
     * To use, call `trace.start()`,
     * then execute the long-running code, filling in any [Trace.Attribute]s and [Trace.Metric]s desired.
     *
     * Call `trace.stop()` when finished.
     */
    inline fun <reified T : Trace> createTrace(builder: TraceBuilder<T>): T {
        val newTrace = SimpleTrace()

        return builder(newTrace)
    }
}

typealias TraceBuilder<T> = (traceInfo: TraceInfo) -> T

abstract class TraceInfo {
    var name: String? = null
    abstract fun start()
    abstract fun stop()
    abstract val attributes: MutableMap<String, String>
    abstract val metrics: MutableMap<String, Long>
}

class SimpleTrace : TraceInfo() {
    private var startTime: Date? = null
    private var endTime: Date? = null

    override fun start() {
        startTime = Date()
    }

    override fun stop() {
        endTime = Date()
        Timber.i {
            "---\nTrace \"$name\" complete.\n" +
                    "Run time: ${endTime!!.time - startTime!!.time} millis\n" +
                    if (attributes.any()) "Attributes: $attributes\n" else "" +
                            if (metrics.any()) "Metrics: $metrics" else ""
        }
    }

    override val attributes: MutableMap<String, String> = mutableMapOf()
    override val metrics: MutableMap<String, Long> = mutableMapOf()
}