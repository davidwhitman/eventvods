package com.eventvods.api

import com.eventvods.api.models.Event
import com.eventvods.api.models.EventSummary
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

internal interface EventvodsService {
    @GET("events")
    fun listEvents(): Call<List<EventSummary>>

    @GET("events/slug/{eventReferenceName}")
    fun getEvent(@Path("eventReferenceName") eventReferenceName: String): Call<Event>
}