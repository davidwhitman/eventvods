package com.eventvods.api

import com.eventvods.api.models.Event
import com.eventvods.api.models.EventSummary
import com.eventvods.api.util.ZonedDateTimeConverter
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import org.threeten.bp.ZonedDateTime
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import kotlin.coroutines.suspendCoroutine
import kotlin.coroutines.resume

class EventvodsApi(okClient: OkHttpClient = OkHttpClient()) {

    private val client = Retrofit.Builder()
        .baseUrl("https://eventvods.com/api/")
        .client(okClient)
        .addConverterFactory(
            GsonConverterFactory.create(
                GsonBuilder().registerTypeAdapter(
                    ZonedDateTime::class.java,
                    ZonedDateTimeConverter()
                ).create()
            )
        )
        .build()
        .create(EventvodsService::class.java)

    suspend fun listEvents(): List<EventSummary> =
        suspendCoroutine { it.resume(client.listEvents().execute().body()!!) }

    suspend fun getEvent(eventReference: String): Event =
        suspendCoroutine { it.resume(client.getEvent(eventReference).execute().body()!!) }
}