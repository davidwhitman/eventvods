package com.eventvods.api.models

import com.google.gson.annotations.SerializedName
import org.threeten.bp.ZonedDateTime
import java.io.Serializable

data class EventSummary(
    @SerializedName("_id") var id: String?,
    @SerializedName("updatedAt") var updatedAt: ZonedDateTime,
    @SerializedName("name") var name: String,
    @SerializedName("game") var game: GameSummary,
    @SerializedName("slug") var referenceName: String,
    @SerializedName("subtitle") var subtitle: String,
    @SerializedName("startDate") var startDate: ZonedDateTime,
    @SerializedName("endDate") var endDate: ZonedDateTime,
    @SerializedName("logo") var logoUri: String,
    @SerializedName("status") var status: String,
    @SerializedName("updated") var updated: String,
    @SerializedName("progress_days") var progress: EventProgress,
    @SerializedName("followers") var followers: Int
) : Serializable

data class GameSummary(
    @SerializedName("slug") var referenceName: String
) : Serializable