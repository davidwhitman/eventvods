package com.eventvods.api.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class EventProgress(
        @SerializedName("msg") var message: String,
        @SerializedName("percentage") var percentage: Int) : Serializable