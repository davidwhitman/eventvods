package com.eventvods.api.models

import com.google.gson.annotations.SerializedName
import org.threeten.bp.ZonedDateTime
import java.io.Serializable
import java.util.*

/**
 * A show that is put on, generally over multiple days, by a host. Usually consists of multiple
 * stages, with each stage taking place over multiple days, and each day having multiple series of games.
 */
data class Event(
        @SerializedName("_id") val id: String?,
        @SerializedName("updatedAt") val updatedAt: ZonedDateTime?,
        @SerializedName("createdAt") val createdAt: ZonedDateTime,
        @SerializedName("logo") val logoUri: String,
        @SerializedName("name") val name: String,
        @SerializedName("shortTitle") val shortTitle: String,
        @SerializedName("subtitle") val subtitle: String,
        @SerializedName("slug") val slug: String,
        @SerializedName("game") val game: Game,
        @SerializedName("patch") val patch: String,
        @SerializedName("format") val format: String,
        @SerializedName("twitchStream") val twitchStream: String,
        @SerializedName("youtubeStream") val youtubeStream: String,
        @SerializedName("startDate") val startDate: ZonedDateTime,
        @SerializedName("endDate") val endDate: ZonedDateTime,
        @SerializedName("credits") val credits: String,
        @SerializedName("redditThreads") val redditThreads: List<String>?,
        @SerializedName("contents") val contents: List<Content>?,
        @SerializedName("teams") val teams: ArrayList<Team>?,
        @SerializedName("media") val media: ArrayList<Media>?,
        @SerializedName("staff") val staff: ArrayList<Staff>?,
        @SerializedName("status") val status: String,
        @SerializedName("updated") val updated: String,
        @SerializedName("progressDays") val progressDays: EventProgress?,
        @SerializedName("followers") val followers: Int) : Serializable

/**
 * The section of content for the event, which can contain multiple modules (eg Group Stage, Knockout stage).
 */
data class Content(
        @SerializedName("_id") val id: String,
        @SerializedName("title") val title: String,
        @SerializedName("modules") val modules: List<Module>?) : Serializable

/**
 * The data of one specific part of an event (eg day 1, day 2, etc).
 */
data class Module(
        @SerializedName("_id") val id: String,
        @SerializedName("date") val date: ZonedDateTime?,
        @SerializedName("title") val title: String,
        @SerializedName("twitch") val twitch: Boolean,
        @SerializedName("youtube") val youtube: Boolean,
        @SerializedName("matches") val matchIds: List<String>?,
        @SerializedName("matches2") val matches: List<Matches>?) : Serializable

/**
 * The game that is played (eg LoL, DotA, Overwatch).
 */
data class Game(
        @SerializedName("_id") val id: String,
        @SerializedName("name") val name: String,
        @SerializedName("slug") val slug: String,
        @SerializedName("icon") val icon: String,
        @SerializedName("credits") val credits: String,
        @SerializedName("subreddit") val subreddit: String) : Serializable

/**
 * A team.
 */
data class Team(
        @SerializedName("_id") val id: String,
        @SerializedName("name") val name: String,
        @SerializedName("tag") val tag: String,
        @SerializedName("slug") val slug: String,
        @SerializedName("icon") val icon: String) : Serializable

/**
 * Link to a vod on YouTube.
 */
data class Youtube(
        @SerializedName("gameStart") val gameStart: String,
        @SerializedName("picksBans") val picksBans: String) : Serializable

/**
 * Link to a vod on Twitch.
 */
data class Twitch(
        @SerializedName("picksBans") val picksBans: String) : Serializable

/**
 * Info on a specific match between teams.
 */
data class MatchInfo(
        @SerializedName("_id") val id: String,
        @SerializedName("rating") val rating: Double,
        @SerializedName("placeholder") val placeholder: Boolean,
        @SerializedName("youtube") val youtube: Youtube?,
        @SerializedName("twitch") val twitch: Twitch?,
        @SerializedName("links") val links: List<String>?) : Serializable

/**
 * Info on a match series (best of 1, 3, 5, etc)
 */
data class Matches(
        @SerializedName("_id") val id: String,
        @SerializedName("team1") val team1: Team?,
        @SerializedName("team2") val team2: Team?,
        @SerializedName("v") val v: Int,
        @SerializedName("date") val date: ZonedDateTime?,
        @SerializedName("data") val matchInfos: List<MatchInfo>?,
        @SerializedName("spoiler2") val spoiler2: Boolean,
        @SerializedName("spoiler1") val spoiler1: Boolean,
        @SerializedName("bestOf") val bestOf: Int,
        @SerializedName("discussionIndex") val discussionIndex: Int,
        @SerializedName("statsIndex") val statsIndex: Int,
        @SerializedName("highlightsIndex") val highlightsIndex: Int) : Serializable

/**
 * Official media for the event.
 */
data class Media(
        @SerializedName("_id") val id: String,
        @SerializedName("name") val name: String,
        @SerializedName("link") val link: String,
        @SerializedName("type") val type: String) : Serializable

/**
 * The staff involved in the event.
 */
data class Staff(
        @SerializedName("_id") val id: String,
        @SerializedName("v") val v: Int,
        @SerializedName("photo") val photo: String,
        @SerializedName("role") val role: String,
        @SerializedName("slug") val slug: String,
        @SerializedName("alias") val alias: String,
        @SerializedName("surname") val surname: String,
        @SerializedName("forename") val forename: String,
        @SerializedName("media") val media: List<Media>?,
        @SerializedName("name") val name: String) : Serializable