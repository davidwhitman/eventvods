package com.eventvods.api.util

import android.net.Uri
import okhttp3.OkHttpClient
import okhttp3.Request

class UrlRedirectFollower(private val okHttpClient: OkHttpClient) {
    @Throws(Exception::class)
    suspend fun getFinalUrl(url: String): Uri? {
        val response = okHttpClient.newCall(Request.Builder().url(url).get().build()).execute()

        return Uri.parse(response.request().url().uri().toString())
    }
}