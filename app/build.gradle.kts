import org.jetbrains.kotlin.gradle.dsl.Coroutines

plugins {
    id("com.android.application")
    id("kotlin-android")
    id("kotlin-android-extensions")
    id("kotlin-kapt")
}

buildscript {
    repositories {
        jcenter()
    }
//    dependencies {
//        classpath("com.akaita.android:easylauncher:1.2.0")
//    }
}

//apply(plugin = "com.akaita.android.easylauncher")

android {
    compileSdkVersion(28)
    defaultConfig {
        applicationId ="com.eventvods.viewer"
        minSdkVersion(24)
        targetSdkVersion(28)
        versionCode = 1
        versionName ="1.0"
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    signingConfigs {
        getByName("debug") {
            storeFile = file("debugKeystore.jks")
            storePassword = "eventvods"
            keyAlias = "key0"
            keyPassword = "eventvods"
        }
    }

    buildTypes {
        getByName("debug") {
            isDebuggable = true
            isMinifyEnabled = false
            applicationIdSuffix = ".debug"
        }

        getByName("release") {
            isMinifyEnabled = true
            signingConfig = signingConfigs.getByName("debug")
            proguardFiles(getDefaultProguardFile("proguard-android.txt"), "proguard-rules.pro")
        }
    }

    packagingOptions {
        pickFirst("META-INF/atomicfu.kotlin_module")
    }
}

dependencies {
    val kotlin_version: String by rootProject.extra
    val coroutines_version: String by rootProject.extra
    implementation(project(":api"))
    implementation(project(":logging"))
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk7:$kotlin_version")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-android:$coroutines_version")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:$coroutines_version")

    implementation("androidx.appcompat:appcompat:1.0.2")
    implementation("androidx.recyclerview:recyclerview:1.0.0")
    implementation("androidx.vectordrawable:vectordrawable-animated:1.0.0")
    implementation("androidx.exifinterface:exifinterface:1.0.0")
    implementation("com.google.android.material:material:1.0.0")
    implementation("androidx.constraintlayout:constraintlayout:2.0.0-beta1")
    implementation("androidx.lifecycle:lifecycle-extensions:2.0.0")
    kapt("androidx.lifecycle:lifecycle-compiler:2.0.0")

    implementation("com.jaeger.statusbarutil:library:1.5.1")

    // Non-native YouTube player option
    implementation("com.pierfrancescosoffritti.androidyoutubeplayer:core:10.0.3")

    implementation("com.google.code.gson:gson:2.8.4")
    implementation("com.jakewharton.threetenabp:threetenabp:1.2.0")

    // NoSQL database
    implementation("io.paperdb:paperdb:2.6")

    implementation("ru.terrakok.cicerone:cicerone:3.0.0")

    implementation("com.github.ajalt:timberkt:1.4.0")
    implementation("com.airbnb.android:epoxy:3.4.2")
    implementation("com.squareup.picasso:picasso:2.71828")
    debugImplementation("com.readystatesoftware.chuck:library:1.1.0")
    releaseImplementation("com.readystatesoftware.chuck:library-no-op:1.1.0")

    testImplementation("junit:junit:4.12")
    testImplementation("androidx.arch.core:core-testing:2.0.1")

    androidTestImplementation("androidx.test:runner:1.2.0-beta01")
    androidTestImplementation("androidx.test.espresso:espresso-core:3.2.0-beta01")
}

kotlin {
    experimental {
        coroutines = Coroutines.ENABLE
    }
}