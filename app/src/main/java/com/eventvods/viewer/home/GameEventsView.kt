package com.eventvods.viewer.home

import android.graphics.PorterDuff
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.text.Html
import android.text.format.DateUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.airbnb.epoxy.SimpleEpoxyController
import com.eventvods.api.models.EventSummary
import com.eventvods.viewer.*
import com.eventvods.viewer.utils.KotlinEpoxyModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_home_event.view.*
import kotlinx.android.synthetic.main.view_game_events.*
import kotlinx.android.synthetic.main.view_game_events.view.*
import kotlinx.coroutines.*
import java.util.*
import kotlin.coroutines.CoroutineContext
import kotlin.math.roundToInt


class GameEventsView : androidx.fragment.app.Fragment(), CoroutineScope, BaseView<GameEventsViewModel> {

    private val itemsSection = SimpleEpoxyController()
    private var gameId = ""
    private val database = Di.instance.eventSummaryDb
    private lateinit var job: Job

    lateinit var viewModel: GameEventsViewModel

    companion object {
        private const val KEY_GAME = "gameId"

        fun newInstance(gameId: String) = GameEventsView().apply {
            this.arguments = Bundle().apply {
                putString(KEY_GAME, gameId)
            }
        }
    }

    override val coroutineContext: CoroutineContext
        get() = job

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
        inflater.inflate(R.layout.view_game_events, container, false)

    private lateinit var itemBackgroundDrawable: Drawable

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        job = Job()

        gameId = arguments?.getString(KEY_GAME, "") ?: ""

        itemBackgroundDrawable = ColorDrawable(
            activity!!.getColorCompat(
                secondaryGameColors[gameId]
                    ?: R.color.semi_transparent_background
            )
        )
        val backgroundColor = activity!!.getColorCompat(
            gameColors[gameId]
                ?: R.color.backgroundNight
        )

        gameEvents_layout.setBackgroundColor(backgroundColor)

        viewModel = ViewModelProviders.of(this).get(GameEventsViewModel::class.java)
        bindToViewModel(viewModel)

        view.gameEvents_recyclerView.apply {
            val layoutManager = androidx.recyclerview.widget.LinearLayoutManager(activity)
            this.layoutManager = layoutManager
            adapter = itemsSection.adapter
            addItemDecoration(VerticalSpaceItemDecoration(resources.getDimensionPixelOffset(R.dimen.spacingSmall)))
        }

        view.gameEvents_gameIcon.setImageDrawable(
            activity!!.getDrawable(
                gameIcons[gameId]
                    ?: R.drawable.icon_fullscreen
            )
        )

        viewModel.load(gameId)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        job.cancel()
    }

    private fun bindToViewModel(viewModel: GameEventsViewModel) {
        viewModel.init(gameId)

        viewModel.observableState.observe(this, Observer<GameEventsViewModel.GameEventState> {
            render(it!!)
        })
    }

    private fun render(gameEventsState: GameEventsViewModel.GameEventState) {
        if (gameEventsState.eventReferences != null) {
            launch {
                val eventsToShow = gameEventsState.eventReferences.mapNotNull { database.read(it) }

                withContext(Dispatchers.Main) {
                    itemsSection.setModels(eventsToShow.map {
                        EventItem(it, itemBackgroundDrawable)
                            .run {
                                withClickListener {
                                    viewModel.clickOnEvent(this.eventSummary.referenceName)
                                }
                            }
                    })
                }
            }
        }
    }

    private data class EventItem(val eventSummary: EventSummary, private val backgroundDrawable: Drawable) :
        KotlinEpoxyModel(layout = R.layout.item_home_event, id = eventSummary.referenceName) {
        override fun bind(view: View) {
            super.bind(view)
            view.item_event_name.text = eventSummary.name
            view.item_event_subtitle.text = eventSummary.subtitle
            val context = view.context
            view.itemEvent_dates.text = DateUtils.formatDateRange(
                context,
                eventSummary.startDate.toLocalDateTime().time,
                eventSummary.endDate.toLocalDateTime().time,
                DateUtils.FORMAT_SHOW_YEAR or DateUtils.FORMAT_ABBREV_MONTH
            )
            view.itemEvent_lastUpdated.text = Html.fromHtml(
                context.getText(R.string.lastUpdated).toString() + " <b>" +
                        DateUtils.getRelativeTimeSpanString(
                            eventSummary.updatedAt.toLocalDateTime().time,
                            Date().time,
                            DateUtils.MINUTE_IN_MILLIS,
                            0
                        ) + "</b>", Html.FROM_HTML_MODE_LEGACY
            )
            Picasso.get()
                .load(eventSummary.logoUri.ifEmpty { null })
                .into(view.item_event_logo)

            val progress = Date().time - eventSummary.startDate.toLocalDateTime().time
            val total = eventSummary.endDate.toLocalDateTime().time - eventSummary.startDate.toLocalDateTime().time
            view.itemEvent_progressBar.progress = (progress.toDouble() / total * 100).roundToInt()
            view.item_background.background.setTint(
                context.getColorCompat(
                    secondaryGameColors[eventSummary.game.referenceName]
                        ?: R.color.semi_transparent_background
                )
            )
            view.item_background.background.setTintMode(PorterDuff.Mode.SRC_IN)
        }
    }
}