package com.eventvods.viewer.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.PopupMenu
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.eventvods.viewer.BaseView
import com.eventvods.viewer.R
import com.eventvods.viewer.supportedGames
import com.jaeger.library.StatusBarUtil
import kotlinx.android.synthetic.main.view_home.*
import kotlinx.android.synthetic.main.view_home.view.*


class HomeView : androidx.fragment.app.Fragment(), BaseView<HomeViewModel> {
    companion object {
        fun newInstance() = HomeView()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.view_home, container, false)

    lateinit var viewModel: HomeViewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(HomeViewModel::class.java)

        bindToViewModel(viewModel, view)

        view.home_refreshLayout.isEnabled = false
        view.home_refreshLayout.setOnRefreshListener { viewModel.reload() }


        view.home_viewPager.addOnPageChangeListener(object : androidx.viewpager.widget.ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
            }

            override fun onPageSelected(position: Int) {
//                activity!!.window.statusBarColor = activity!!.getColorCompat(R.color.transparent)//gameColors.values.toTypedArray()[position])
            }
        })

        home_menu.setOnClickListener { v ->
            PopupMenu(activity, v).apply {
                inflate(R.menu.home)
                setOnMenuItemClickListener {
                    when (it.itemId) {
                        R.id.homeMenu_refresh -> viewModel.reload()
                    }
                    true
                }
            }.show()
        }

        viewModel.load()
    }

    override fun onResume() {
        super.onResume()
        requireActivity().window.setFlags(
            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)
    }

    override fun onPause() {
        requireActivity().window.clearFlags(
            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)
        super.onPause()
    }

    private fun bindToViewModel(viewModel: HomeViewModel, view: View) {
        // Might have a problem because passing in `args` instead of `savedInstanceState`?
//        intentionSender = viewModel.createIntentionReceiver(Dispatchers.Main)
        viewModel.observableState.observe(this, Observer<HomeViewModel.HomeState> {
            render(it!!, view)
        })
    }

    private fun render(homeState: HomeViewModel.HomeState, view: View) {
        view.home_refreshLayout.isRefreshing = homeState.isLoading

//        if (homeState.error == null) {
//            view.home_error.visibility = View.GONE
//        } else {
//            view.home_error.text = homeState.error
//            view.home_error.visibility = View.VISIBLE
//        }

        if (view.home_viewPager.adapter == null && homeState.gameTypesRepresented.isNotEmpty()) {
            view.home_viewPager.adapter =
                object : androidx.fragment.app.FragmentStatePagerAdapter(childFragmentManager) {
                    override fun getItem(position: Int): androidx.fragment.app.Fragment =
                        GameEventsView.newInstance(supportedGames[position])

                    override fun getCount(): Int = supportedGames.size
                }
        }
    }
}