package com.eventvods.viewer.home

import com.eventvods.api.models.EventSummary
import com.eventvods.viewer.BaseViewModel
import com.eventvods.viewer.Di
import com.eventvods.viewer.State
import com.github.ajalt.timberkt.Timber
import com.strider.logging.Trace
import kotlinx.coroutines.launch
import java.util.*

class HomeViewModel : BaseViewModel<HomeViewModel.HomeState>() {
    private val eventSummaryDatabase = Di.instance.eventSummaryDb
    private val eventvodsApi = Di.instance.eventVodsApi
    private val tracer = Di.instance.tracer

    data class HomeState(
        val isLoading: Boolean = false,
        val error: String? = null,
        val gameTypesRepresented: List<String> = emptyList()
    ) : State

    override val initialState = HomeState()

    fun load(forceRefresh: Boolean = false) {
        launch {
            emitState { it.copy(isLoading = true) }

            try {
                val eventSummaries = getEventSummariesAsap(forceRefresh)
                showSummaries(eventSummaries)
            } catch (e: Exception) {
                Timber.e(e)
                loadFailed(e.message ?: "")
            }
        }
    }

    fun reload() = load(forceRefresh = true)

    private fun showSummaries(eventSummaries: List<EventSummary>) {
        emitState { state ->
            state.copy(
                isLoading = false,
                error = null,
                gameTypesRepresented = eventSummaries.distinctBy { it.game.referenceName }.map { it.referenceName })
        }
    }

    private fun loadFailed(error: String) {
        emitState { it.copy(isLoading = false, error = error) }
    }

    /**
     * First tries to get data from the database. If that fails (or forceRefresh is true), then get data from the API
     * and put it into the database.
     */
    private suspend fun getEventSummariesAsap(forceRefresh: Boolean = false): List<EventSummary> {
        if (!forceRefresh) {
            tracer.runTraceWithResult({ Trace.ReadEventSummariesFromDbTrace(it) }) {
                val dbData = eventSummaryDatabase.readAll()
                it.itemCount = dbData.size.toLong()

                if (dbData.isNotEmpty()) {
                    Timber.v { "Loading events from database." }
                    return dbData
                }
            }
        }

        Timber.v { "Loading events from api." }
        val freshData = eventvodsApi.listEvents()

        val started = Date().time
        eventSummaryDatabase.writeAll(freshData) { it.referenceName }
        Timber.v { "Writing ${freshData.size} items took ${Date().time - started} millis." }

        return freshData
    }
}