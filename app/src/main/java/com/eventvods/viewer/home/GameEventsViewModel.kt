package com.eventvods.viewer.home

import com.eventvods.viewer.*
import com.github.ajalt.timberkt.Timber
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.*

class GameEventsViewModel :
    BaseViewModel<GameEventsViewModel.GameEventState>() {
    private val router = Di.instance.router
    private val eventSummaryDatabase = Di.instance.eventSummaryDb

    private lateinit var gameId: String

    data class GameEventState(
        val game: String = "",
        val eventReferences: List<String>? = null
    ) : State

    override val initialState = GameEventState()

    fun init(gameId: String) {
        this.gameId = gameId
    }

    fun load(gameId: String) {
        launch {
            val startedLoadingAt = Date().time
            show(eventSummaryDatabase.readAll()
                .asSequence()
                .filter { it.game.referenceName == gameId }
                .sortedByDescending { it.updatedAt.toLocalDateTime().time }
                .map { it.referenceName }
                .toList())
            Timber.v { "Loading events for $gameId took ${Date().time - startedLoadingAt} millis." }
        }
    }

    fun clickOnEvent(eventReference: String) {
        launch {
            withContext(mainContext) {
                router.navigateToEventScreen(
                    EventScreenParams(
                        eventReference = eventReference,
                        gameId = gameId
                    )
                )
            }
        }
    }

    private fun show(eventReferences: List<String>) {
        emitState { it.copy(game = gameId, eventReferences = eventReferences) }
    }
}