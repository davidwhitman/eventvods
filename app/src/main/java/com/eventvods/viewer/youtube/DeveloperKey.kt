// Copyright 2014 Google Inc. All Rights Reserved.

package com.eventvods.viewer.youtube

/**
 * Static container class for holding a reference to your YouTube Developer Key.
 */
object DeveloperKey {
    const val DEVELOPER_KEY = "AIzaSyC8OLdkG8EFlVKlDRq8NrgHm9w_hUsp0H0"
}
