package com.eventvods.viewer.youtube

import android.net.Uri
import com.eventvods.api.util.UrlRedirectFollower
import com.eventvods.viewer.Di
import com.github.ajalt.timberkt.Timber

class LinkParsers {
    /**
     * First tries known link patterns, then tries to follow url redirects and checks again for known patterns.
     */
    suspend fun parse(uri: String): YoutubeWatchInfo? {
        fun tryParse(uri: String) = parseFullLink(uri)
            ?: parseShortLink(uri)

        return tryParse(uri) ?: tryParse(followRedirects(uri).toString())
    }

    /**
     * Tries to parse links with the pattern https://youtube.com?v=23asdf4f&t=12h11m49s
     */
    private fun parseFullLink(rawUri: String): YoutubeWatchInfo? {
        val uri = try {
            Uri.parse(rawUri) ?: Uri.EMPTY
        } catch (e: Exception) {
            return null
        }

        if (uri.host?.contains("youtube") != true) {
            return null
        }

        val id = uri.getQueryParameter("v")

        if (id == null) {
            val error = IllegalArgumentException("YouTube video missing id! $uri")
            Timber.d(error)
            return null
        }

        val time = uri.getQueryParameter("t")

        val seconds = if (time != null) {
            parseTimeToSeconds(time)
        } else {
            0
        }

        return YoutubeWatchInfo(id, seconds)
    }


    /**
     * Tries to parse links with the pattern https://youtu.be/23asdf4f?t=12h11m49s
     */
    private fun parseShortLink(rawUri: String): YoutubeWatchInfo? {
        val uri = try {
            Uri.parse(rawUri) ?: Uri.EMPTY
        } catch (e: Exception) {
            return null
        }

        if (uri.host?.contains("youtu.be") != true) {
            return null
        }

        val id = uri.pathSegments.getOrNull(0)

        if (id == null) {
            val error = IllegalArgumentException("YouTube video missing id! $uri")
            Timber.d(error)
            return null
        }

        val time = uri.getQueryParameter("t")

        val seconds = if (time != null) {
            parseTimeToSeconds(time)
        } else {
            0
        }

        return YoutubeWatchInfo(id, seconds)
    }

    /**
     * If the url can't be parsed, maybe it has gone through a url shortener. Try following it and grab the redirected url.
     */
    private suspend fun followRedirects(url: String): Uri? {
        return try {
            val followedUrl = UrlRedirectFollower(Di.instance.apiClient).getFinalUrl(url)

            if (followedUrl != null && followedUrl.toString().isNotBlank()) {
                followedUrl
            } else {
                Timber.d(IllegalArgumentException("Unable to parse Youtube url: $url, even after following it to $followedUrl"))
                null
            }
        } catch (e: Exception) {
            Timber.d(IllegalArgumentException("Unable to parse Youtube url: $url"))
            null
        }
    }

    private fun parseTimeToSeconds(time: String): Int {
        val times = Regex("^([0-9]+h)?([0-9]+m)?([0-9]+s)?")
            .find(time)
            ?.groupValues
            ?.drop(1) // First group is always the full matched string (eg 10m15s), throw that out
            ?.filter { it.isNotBlank() } // Remove blank results (like if the timestamp doesn't contain hours)

        var millis = 0

        if (times != null) {
            for (match in times) {
                millis += when (match.last()) {
                    's' -> match.removeSuffix("s").toInt()
                    'm' -> match.removeSuffix("m").toInt() * 60
                    'h' -> match.removeSuffix("h").toInt() * 60 * 60
                    else -> 0
                }
            }
        }

        return millis
    }
}

data class YoutubeWatchInfo(val videoId: String, val startTimeInSeconds: Int)