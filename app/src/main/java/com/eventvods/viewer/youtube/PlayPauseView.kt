package com.eventvods.viewer.youtube

import android.animation.AnimatorSet
import android.animation.ArgbEvaluator
import android.animation.ObjectAnimator
import android.annotation.TargetApi
import android.content.Context
import android.graphics.Canvas
import android.graphics.Outline
import android.graphics.Paint
import android.graphics.drawable.Drawable
import android.os.Build
import android.util.AttributeSet
import android.util.Property
import android.view.View
import android.view.ViewOutlineProvider
import android.view.animation.DecelerateInterpolator
import android.widget.FrameLayout
import com.eventvods.viewer.R

/**
 * [https://github.com/alexjlockwood/adp-path-morph-play-to-pause/blob/master/app/src/main/java/com/alexjlockwood/example/playpauseanimation/PlayPauseDrawable.java]
 */
class PlayPauseView(context: Context, attrs: AttributeSet) : FrameLayout(context, attrs) {
    private val drawable: PlayPauseDrawable
    private val paint = Paint()
    private val pauseBackgroundColor: Int
    private val playBackgroundColor: Int

    private var animatorSet: AnimatorSet? = null
    private var mBackgroundColor: Int = 0
    private var mWidth: Int = 0
    private var mHeight: Int = 0

    private var color: Int
        get() = mBackgroundColor
        set(color) {
            mBackgroundColor = color
            invalidate()
        }

    init {
        setWillNotDraw(false)
        mBackgroundColor = resources.getColor(R.color.transparent)
        paint.isAntiAlias = true
        paint.style = Paint.Style.FILL
        drawable = PlayPauseDrawable(context)
        drawable.callback = this

        pauseBackgroundColor = resources.getColor(R.color.transparent)
        playBackgroundColor = resources.getColor(R.color.transparent)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        val size = Math.min(measuredWidth, measuredHeight)
        setMeasuredDimension(size, size)
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        drawable.setBounds(0, 0, w, h)
        mWidth = w
        mHeight = h

        outlineProvider = object : ViewOutlineProvider() {
            @TargetApi(Build.VERSION_CODES.LOLLIPOP)
            override fun getOutline(view: View, outline: Outline) {
                outline.setOval(0, 0, view.width, view.height)
            }
        }

        clipToOutline = true
    }

    override fun verifyDrawable(who: Drawable): Boolean {
        return who === drawable || super.verifyDrawable(who)
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        paint.color = mBackgroundColor
        val radius = Math.min(mWidth, mHeight) / 2f
        canvas.drawCircle(mWidth / 2f, mHeight / 2f, radius, paint)
        drawable.draw(canvas)
    }

    fun toggle(playing: Boolean = drawable.isPlay) {
        if (animatorSet != null) {
            animatorSet!!.cancel()
        }

        animatorSet = AnimatorSet()
        val colorAnim = ObjectAnimator.ofInt(this, COLOR, if (playing) pauseBackgroundColor else playBackgroundColor)
        drawable.isPlay = playing
        colorAnim.setEvaluator(ArgbEvaluator())
        val pausePlayAnim = drawable.pausePlayAnimator
        animatorSet!!.interpolator = DecelerateInterpolator()
        animatorSet!!.duration = PLAY_PAUSE_ANIMATION_DURATION
        animatorSet!!.playTogether(colorAnim, pausePlayAnim)
        animatorSet!!.start()
    }

    companion object {

        private val COLOR = object : Property<PlayPauseView, Int>(Int::class.java, "color") {
            override fun get(v: PlayPauseView): Int {
                return v.color
            }

            override fun set(v: PlayPauseView, value: Int) {
                v.color = value
            }
        }

        private val PLAY_PAUSE_ANIMATION_DURATION: Long = 200
    }
}