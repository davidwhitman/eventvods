package com.eventvods.viewer.youtube

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.eventvods.viewer.Di
import com.eventvods.viewer.R
import com.eventvods.viewer.database.MatchTracker
import com.github.ajalt.timberkt.Timber
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.PlayerConstants
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener
import kotlinx.android.synthetic.main.activity_youtube_webview_player.*
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

class YouTubeWebviewPlayerActivity : AppCompatActivity(), CoroutineScope {
    val userWatchProgressBook = Di.instance.userWatchProgressDb
    val matchTracker = MatchTracker(userWatchProgressBook)
    private var youtubePlayer: YouTubePlayer? = null
    private val linkParsers = LinkParsers()

    override var coroutineContext: CoroutineContext = Dispatchers.Default

    lateinit var matchId: String

    companion object {
        private const val PROGRESS_SAVE_INTERVAL_IN_SECONDS = 5

        private const val KEY_URL = "key_url"
        private const val KEY_MATCH_ID = "key_match_id"

        fun createIntent(context: Context, youtubeUrl: String, matchId: String): Intent =
            Intent(context, YouTubeWebviewPlayerActivity::class.java).apply {
                putExtra(KEY_URL, youtubeUrl)
                putExtra(KEY_MATCH_ID, matchId)
            }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_youtube_webview_player)
        coroutineContext = Dispatchers.Default + Job()

        if (!intent.hasExtra(KEY_URL) || intent.getStringExtra(KEY_URL).isBlank()) {
            Timber.d(
                IllegalArgumentException(),
                { "Launched ${YouTubeWebviewPlayerActivity::class.java.simpleName} without url." })
            return
        }

        if (!intent.hasExtra(KEY_MATCH_ID) || intent.getStringExtra(KEY_MATCH_ID).isBlank()) {
            Timber.d(
                IllegalArgumentException(),
                { "Launched ${YouTubeNativePlayerActivity::class.java.simpleName} without match id." })
            return
        }


        player_youtubeView.getPlayerUiController().showSeekBar(false)
        player_youtubeView.getPlayerUiController().showDuration(false)

        matchId = intent.getStringExtra(KEY_MATCH_ID)
        initializePlayer(intent.getStringExtra(KEY_URL))
    }

    override fun onPause() {
        super.onPause()
        youtubePlayer?.pause()
    }

    override fun onResume() {
        super.onResume()
        youtubePlayer?.play()
    }

    override fun onDestroy() {
        super.onDestroy()
        player_youtubeView?.release()
        coroutineContext.cancel()
    }

    private fun initializePlayer(url: String) {
        fun launchVideo(watchInfo: YoutubeWatchInfo) {
            Timber.v { "Launching video with id ${watchInfo.videoId} at time ${watchInfo.startTimeInSeconds} seconds" }
            youtubePlayer?.loadVideo(watchInfo.videoId, watchInfo.startTimeInSeconds.toFloat())
        }

        player_youtubeView.initialize(object : AbstractYouTubePlayerListener() {
            override fun onReady(youTubePlayer: YouTubePlayer) {
                Timber.v { "Initialized YouTube player successfully!" }
                youtubePlayer = youTubePlayer

                launch {
                    var videoInfo = linkParsers.parse(url)


                    if (videoInfo == null) {
                        Timber.w(IllegalArgumentException("Unable to parse Youtube url: $url"))
                        finish()
                    } else {
                        var startTime = videoInfo.startTimeInSeconds

                        startTime = matchTracker.getIdealStartingSecond(matchId, startTime)
                        videoInfo = videoInfo.copy(startTimeInSeconds = startTime)

                        // Delay so that the javascript onPlayerReady event is called
                        delay(500)
                        launchVideo(videoInfo)
                    }
                }
            }

            override fun onError(youTubePlayer: YouTubePlayer, error: PlayerConstants.PlayerError) {
                Timber.e(RuntimeException()) { "Error initializing webview YouTube player. Error: $error" }
            }

            override fun onCurrentSecond(youTubePlayer: YouTubePlayer, second: Float) {
                val currentTimestamp = second.toInt()
                val lastSavedTimestamp = matchTracker.getWatchProgress(matchId)?.secondsWatchedTo

                if (lastSavedTimestamp == null || currentTimestamp - lastSavedTimestamp >= PROGRESS_SAVE_INTERVAL_IN_SECONDS) {
                    matchTracker.saveWatchProgress(matchId, currentTimestamp)
                }
            }
        }, true)
    }
}