package com.eventvods.viewer.youtube

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import com.eventvods.viewer.Di
import com.eventvods.viewer.R
import com.eventvods.viewer.database.MatchTracker
import com.github.ajalt.timberkt.Timber
import com.google.android.youtube.player.YouTubeBaseActivity
import com.google.android.youtube.player.YouTubeInitializationResult
import com.google.android.youtube.player.YouTubePlayer
import com.google.android.youtube.player.YouTubePlayer.*
import kotlinx.android.synthetic.main.activity_youtube_native_player.*
import kotlinx.coroutines.*
import java.util.*
import kotlin.coroutines.CoroutineContext

class YouTubeNativePlayerActivity : YouTubeBaseActivity(), CoroutineScope {
    private val userWatchProgressBook = Di.instance.userWatchProgressDb
    val matchTracker = MatchTracker(userWatchProgressBook)
    var youtubePlayer: YouTubePlayer? = null
    val linkParsers = LinkParsers()
    lateinit var matchId: String
    override var coroutineContext: CoroutineContext = Dispatchers.Default

    companion object {
        private const val RECOVERY_DIALOG_REQUEST = 1
        private const val KEY_URL = "key_url"
        private const val KEY_MATCH_ID = "key_match_id"
        private const val KEY_TIME = "key_time"

        fun createIntent(context: Context, youtubeUrl: String, matchId: String): Intent =
            Intent(context, YouTubeNativePlayerActivity::class.java).apply {
                putExtra(KEY_URL, youtubeUrl)
                putExtra(KEY_MATCH_ID, matchId)
            }
    }

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        if (youtubeNativePlayer_controls.visibility != View.VISIBLE) {
            youtubeNativePlayer_controls.visibility = View.VISIBLE

            launch {
                delay(1800)

                withContext(Dispatchers.Main) {
                    youtubeNativePlayer_controls.visibility = View.GONE
                }
            }

            return true
        }

        return super.dispatchTouchEvent(ev)
    }

    private val timer = Timer()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_youtube_native_player)
        coroutineContext = Dispatchers.Default + Job()

        if (!intent.hasExtra(KEY_URL) || intent.getStringExtra(KEY_URL).isBlank()) {
            Timber.d(
                IllegalArgumentException()
            ) { "Launched ${YouTubeNativePlayerActivity::class.java.simpleName} without url." }
            return
        }

        if (!intent.hasExtra(KEY_MATCH_ID) || intent.getStringExtra(KEY_MATCH_ID).isBlank()) {
            Timber.d(
                IllegalArgumentException()
            ) { "Launched ${YouTubeNativePlayerActivity::class.java.simpleName} without match id." }
            return
        }

        matchId = intent.getStringExtra(KEY_MATCH_ID)
        initializePlayer(intent.getStringExtra(KEY_URL))

//        youtubeNativePlayer_layout.setOnClickListener(toggleIsPlaying())
//        youtubeNativePlayer_playPauseButton.setOnClickListener(toggleIsPlaying())

        timer.scheduleAtFixedRate(object : TimerTask() {
            override fun run() {
                youtubePlayer?.let { player ->
                    intent.putExtra(KEY_TIME, player.currentTimeMillis)
                    matchTracker.saveWatchProgress(matchId, player.currentTimeMillis)
                }
            }
        }, 5000, 1000)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == RECOVERY_DIALOG_REQUEST) {
            initializePlayer(intent.getStringExtra(KEY_URL))
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    override fun onDestroy() {
        timer.cancel()
        youtubePlayer?.release()
        coroutineContext.cancel()
        super.onDestroy()
    }

    private fun initializePlayer(url: String) {
        fun launchVideo(watchInfo: YoutubeWatchInfo) {
            Timber.v { "Launching video with id ${watchInfo.videoId} at time ${watchInfo.startTimeInSeconds} ms" }
            youtubePlayer?.loadVideo(watchInfo.videoId, watchInfo.startTimeInSeconds.toInt())
        }

        youtubeNativePlayer_youtubeView.initialize(
            DeveloperKey.DEVELOPER_KEY,
            object : OnInitializedListener {
                override fun onInitializationSuccess(
                    provider: Provider?,
                    player: YouTubePlayer,
                    wasRestored: Boolean
                ) {
                    Timber.v { "Initialized YouTube player successfully! Restored: $wasRestored" }
                    youtubePlayer = player.apply {
                        this.setPlayerStyle(PlayerStyle.CHROMELESS)
                        this.setPlaybackEventListener(playbackEventListener)
                        this.fullscreenControlFlags =
                            FULLSCREEN_FLAG_CONTROL_ORIENTATION and FULLSCREEN_FLAG_ALWAYS_FULLSCREEN_IN_LANDSCAPE and FULLSCREEN_FLAG_CONTROL_SYSTEM_UI
                        this.setPlayerStateChangeListener(object : PlayerStateChangeListener {
                            override fun onAdStarted() {

                            }

                            override fun onLoading() {
                            }

                            override fun onVideoStarted() {
                                val dialog = android.app.Dialog(
                                    this@YouTubeNativePlayerActivity,
                                    R.style.AppTheme_NoActionBar_Fullscreen
                                )
                                dialog.setContentView(R.layout.dialog_invisible)
                                val view = dialog.findViewById<View>(R.id.dialogInvisible_container)

                                view.setOnTouchListener(object :
                                    OnSwipeTouchListener(this@YouTubeNativePlayerActivity) {
                                    override fun onSingleTouch() {
                                        toggleIsPlaying()
                                    }
                                })
                                dialog.window.setBackgroundDrawable(
                                    android.graphics.drawable.ColorDrawable(
                                        this@YouTubeNativePlayerActivity.getColor(
                                            R.color.transparent
                                        )
                                    )
                                )
                                dialog.setOnDismissListener {
                                    this@YouTubeNativePlayerActivity.finish()
                                }
                                dialog.show()
                            }

                            override fun onLoaded(p0: String?) {
                            }

                            override fun onVideoEnded() {
                            }

                            override fun onError(p0: ErrorReason?) {
                            }

                        })
                    }

                    launch {
                        var videoInfo = linkParsers.parse(url)

                        if (videoInfo == null) {
                            Timber.w(IllegalArgumentException("Unable to parse Youtube url: $url"))
                            finish()
                        } else {
                            var startTime = videoInfo.startTimeInSeconds

                            if (wasRestored && intent.hasExtra(KEY_TIME)) {
                                startTime = intent.getIntExtra(KEY_TIME, 0)
                            }

                            startTime = matchTracker.getIdealStartingSecond(matchId, startTime)
                            videoInfo = videoInfo.copy(startTimeInSeconds = startTime)

                            launchVideo(videoInfo)
                        }
                    }
                }

                override fun onInitializationFailure(
                    provider: Provider?,
                    error: YouTubeInitializationResult
                ) {
                    Timber.e { "Error initializing YouTube player! Result: $error" }

                    if (error.isUserRecoverableError) {
                        error.getErrorDialog(this@YouTubeNativePlayerActivity, RECOVERY_DIALOG_REQUEST).show()
                    }
                }
            })
    }

    private fun toggleIsPlaying() {
        val youtubePlayerLocal = youtubePlayer
        if (youtubePlayerLocal != null) {
            if (youtubePlayerLocal.isPlaying) {
                youtubePlayerLocal.pause()
            } else {
                youtubePlayerLocal.play()
            }
        }
    }

    private val playbackEventListener = object : PlaybackEventListener {
        override fun onSeekTo(p0: Int) {
        }

        override fun onBuffering(p0: Boolean) {
        }

        override fun onPlaying() {
            youtubeNativePlayer_playPauseButton.toggle(true)
        }

        override fun onStopped() {
        }

        override fun onPaused() {
            youtubeNativePlayer_playPauseButton.toggle(false)
        }
    }
}