package com.eventvods.viewer

object Screens {
    const val Home = "home"
    const val Event = "event"
    const val Youtube = "youtube"
}