package com.eventvods.viewer

import android.content.Context
import com.eventvods.api.EventvodsApi
import com.eventvods.viewer.database.Author
import com.eventvods.viewer.database.EventBook
import com.eventvods.viewer.database.EventSummaryBook
import com.eventvods.viewer.database.UserWatchProgressBook
import com.google.gson.Gson
import com.readystatesoftware.chuck.ChuckInterceptor
import com.strider.logging.Tracer
import io.paperdb.Book
import io.paperdb.Paper
import okhttp3.OkHttpClient
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import java.util.concurrent.TimeUnit

/**
 * Too lazy to set up Dagger right now.
 * And I like fast builds.
 */
class Di(
    context: Context,
    private val author: Author = object : Author {
        override fun invoke(title: String): Book = Paper.book(title)
    },
    private val cicerone: Cicerone<Router> = Cicerone.create(),
    val apiClient: OkHttpClient = OkHttpClient.Builder()
        .connectTimeout(10, TimeUnit.SECONDS)
        .addNetworkInterceptor(ChuckInterceptor(context))
        .build(),
    val eventVodsApi: EventvodsApi = EventvodsApi(apiClient),
    val eventSummaryDb: EventSummaryBook = EventSummaryBook(author),
    val eventDb: EventBook = EventBook(author),
    val userWatchProgressDb: UserWatchProgressBook = UserWatchProgressBook(author),
    val tracer: Tracer = Tracer(),
    val router: Router = cicerone.router,
    val navigationHolder: NavigatorHolder = cicerone.navigatorHolder
) {
    companion object {
        /**
         * Singleton instance of the service locator. Set a new one of these for unit tests.
         */
        lateinit var instance: Di
        private const val EVENT_SUMMARY_DATABASE_NAME = "eventSummary.db"
    }
}