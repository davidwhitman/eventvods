package com.eventvods.viewer

/**
 * Created by David Whitman on 13 May, 2018.
 */
const val LoL = "lol"
const val CSGO = "csgo"
const val DotA = "dota"
const val Overwatch = "overwatch"
const val RocketLeague = "rocket-league"

val supportedGames = listOf(
        LoL,
        CSGO,
        DotA,
        Overwatch,
        RocketLeague)

val gameColors = mapOf(
        LoL to R.color.gameLol,
        CSGO to R.color.gameCsgo,
        DotA to R.color.gameDota,
        Overwatch to R.color.gameOverwatch,
        RocketLeague to R.color.gameRocketLeague)

val secondaryGameColors = mapOf(
        LoL to R.color.gameLolSecondary,
        CSGO to R.color.gameCsgoSecondary,
        DotA to R.color.gameDotaSecondary,
        Overwatch to R.color.gameOverwatchSecondary,
        RocketLeague to R.color.gameRocketLeagueSecondary)

val gameIcons = mapOf(
        LoL to R.drawable.icon_lol,
        CSGO to R.drawable.icon_csgo,
        DotA to R.drawable.icon_dota,
        Overwatch to R.drawable.ic_overwatch,
        RocketLeague to R.drawable.icon_rocketleague)