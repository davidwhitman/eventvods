package com.eventvods.viewer

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.appcompat.app.AppCompatActivity
import android.widget.Toast
import com.eventvods.viewer.event.EventView
import com.eventvods.viewer.home.HomeView
import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.android.SupportFragmentNavigator


class MainActivity : AppCompatActivity() {
    private val navigatorHolder = Di.instance.navigationHolder

    private val navigator = object : SupportFragmentNavigator(supportFragmentManager, R.id.controller_container) {
        override fun createFragment(screenKey: String, data: Any?): androidx.fragment.app.Fragment? =
                when (screenKey) {
                    Screens.Home -> HomeView.newInstance()
                    Screens.Event -> (data as EventScreenParams).run { EventView.newInstance(data.eventReference, data.gameId) }
                    else -> null
                }

        override fun showSystemMessage(message: String) {
            Toast.makeText(this@MainActivity, message, Toast.LENGTH_SHORT).show()
        }

        override fun exit() {
            finish()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        if (savedInstanceState == null) {
            Di.instance.router.replaceScreen(Screens.Home)
        }
    }

    override fun onResumeFragments() {
        super.onResumeFragments()
        navigatorHolder.setNavigator(navigator)
    }

    override fun onPause() {
        navigatorHolder.removeNavigator()
        super.onPause()
    }
}

data class EventScreenParams(val eventReference: String, val gameId: String)

fun Router.navigateToEventScreen(eventScreenParams: EventScreenParams) = this.navigateTo(Screens.Event, eventScreenParams)