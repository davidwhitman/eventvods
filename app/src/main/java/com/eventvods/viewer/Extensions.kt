package com.eventvods.viewer

import android.content.Context
import android.text.Html
import androidx.annotation.ColorInt
import androidx.annotation.ColorRes
import androidx.core.content.ContextCompat
import org.threeten.bp.LocalDateTime
import org.threeten.bp.ZoneOffset


@ColorInt
inline fun Context.getColorCompat(@ColorRes color: Int): Int = ContextCompat.getColor(this, color)

val LocalDateTime.time: Long
    get() = this.toInstant(ZoneOffset.UTC).toEpochMilli()

inline fun String.asHtml() = Html.fromHtml(this, Html.FROM_HTML_MODE_COMPACT)