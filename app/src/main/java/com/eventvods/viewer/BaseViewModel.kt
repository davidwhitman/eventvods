package com.eventvods.viewer

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.github.ajalt.timberkt.Timber
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlin.coroutines.CoroutineContext

/**
 * Boilerplate holder for MVI ViewModels.
 */
abstract class BaseViewModel<S : State> : ViewModel(), CoroutineScope {
    abstract val initialState: S

    override val coroutineContext: CoroutineContext
        get() = job + CoroutineExceptionHandler { _, throwable ->
            Timber.e(throwable)
        }

    protected val mainContext = Dispatchers.Main
    private val job = Job()

    private var currentState: S? = null

    private val mutableLiveData = MutableLiveData<S>()

    /**
     * An observable projection of the ViewModel's state.
     */
    val observableState: LiveData<S> = mutableLiveData

    /**
     * Uses the provided function to convert the previous state to a new state synchronously and 'saves' it in `currentState`.
     * Then, asynchronously updates observers on the main thread.
     */
    protected fun emitState(stateModifier: (S) -> S) {
        val newState = stateModifier(currentState ?: initialState)
        currentState = newState
        mutableLiveData.postValue(currentState)
    }
}