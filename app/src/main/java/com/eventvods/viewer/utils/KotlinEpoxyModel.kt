package com.eventvods.viewer.utils

import android.view.View
import androidx.annotation.CallSuper
import androidx.annotation.LayoutRes
import com.airbnb.epoxy.EpoxyModel

/**
 * Represents a `EpoxyRecyclerView` item model.
 * This class allows us to use epoxy models with Kotlin without needing annotation processing or code generation.
 */
abstract class KotlinEpoxyModel(
    @LayoutRes private val layoutRes: Int,
    id: Long
) : EpoxyModel<View>(id) {
    private var view: View? = null
    private var clickListener: View.OnClickListener? = null
    private var longClickListener: View.OnLongClickListener? = null

    constructor(
        layout: Int,
        id: String
    ) : this(layout, 0) {
        this.id(id)
    }

    @CallSuper
    override fun bind(view: View) {
        this.view = view
        view.setOnClickListener(clickListener)
        view.setOnLongClickListener(longClickListener)
    }

    override fun unbind(view: View) {
        this.view = null
    }

    override fun getDefaultLayout() = layoutRes

    fun withClickListener(listener: (view: View?) -> Unit): KotlinEpoxyModel =
        this.apply {
            clickListener = View.OnClickListener { listener(it) }
        }

    fun withLongClickListener(listener: (view: View) -> Boolean): KotlinEpoxyModel =
        this.apply {
            longClickListener = View.OnLongClickListener { listener(it) }
        }
}