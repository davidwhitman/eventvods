package com.eventvods.viewer.utils

import android.graphics.Color
import android.graphics.PorterDuff
import androidx.annotation.ColorInt
import androidx.annotation.ColorRes
import androidx.core.graphics.drawable.DrawableCompat
import android.widget.ImageView
import androidx.core.graphics.ColorUtils

@ColorInt
fun @receiver:ColorRes Int.lighten(fraction: Double = .2): Int {
    fun lightenColor(color: Int, fraction: Double): Int = Math.min(color + color * fraction, 255.0).toInt()

    return Color.argb(
            Color.alpha(this),
            lightenColor(Color.red(this), fraction),
            lightenColor(Color.green(this), fraction),
            lightenColor(Color.blue(this), fraction)
    )
}

@ColorInt
fun @receiver:ColorRes Int.darken(fraction: Double = .2): Int {
    fun darkenColor(color: Int, fraction: Double): Int = Math.max(color - color * fraction, 0.0).toInt()

    return Color.argb(
            Color.alpha(this),
            darkenColor(Color.red(this), fraction),
            darkenColor(Color.green(this), fraction),
            darkenColor(Color.blue(this), fraction)
    )
}

@ColorInt
fun @receiver:ColorInt Int.withTransparency(alpha: Int = 127): Int {
    fun transparantizeColor(color: Int, alpha: Int): Int = ColorUtils.setAlphaComponent(this, alpha)

    return transparantizeColor(this, alpha)
}

object ColorUtils {
    fun recolor(view: ImageView, @ColorInt color: Int, mode: PorterDuff.Mode) {
        val wrappedDrawable = DrawableCompat.wrap(view.drawable)
        DrawableCompat.setTint(wrappedDrawable, color)
        DrawableCompat.setTintMode(wrappedDrawable, mode)

        view.setImageDrawable(wrappedDrawable)
    }
}