package com.eventvods.viewer.event

import com.eventvods.api.models.Event
import com.eventvods.viewer.BaseViewModel
import com.eventvods.viewer.Di
import com.eventvods.viewer.State
import com.github.ajalt.timberkt.Timber
import kotlinx.coroutines.launch

class EventViewModel : BaseViewModel<EventViewModel.EventState>() {

    data class EventState(
        val isLoading: Boolean = false,
        val error: String? = null,
        val event: Event? = null
    ) : State

    override val initialState = EventState()
    private val eventvodsApi = Di.instance.eventVodsApi
    private val eventDb = Di.instance.eventDb

    fun load(eventReference: String) {
        launch {
            if (eventReference.isBlank()) {
                loadFailed("Event reference cannot be empty or blank!")
            }

            try {
                if (eventDb.contains(eventReference)) {
                    loadedFromCache(eventDb.read(eventReference)!!)
                }
            } catch (e: Exception) {
                Timber.e(e)
            }

            loadingFromNetwork()

            try {
                val event = eventvodsApi.getEvent(eventReference)
                loadedFromNetwork(event)
                eventDb.write(eventReference, event)
            } catch (e: Exception) {
                Timber.e(e)
                loadFailed(e.message ?: "")
            }
        }
    }


    private fun loadingFromNetwork() {
        emitState { it.copy(isLoading = true) }
    }

    private fun loadedFromCache(event: Event) {
        emitState { it.copy(event = event, error = null) }
    }

    private fun loadedFromNetwork(event: Event) {
        emitState { it.copy(isLoading = false, event = event, error = null) }
    }

    private fun loadFailed(error: String) {
        emitState { it.copy(isLoading = false, error = error) }
    }
}