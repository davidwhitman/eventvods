package com.eventvods.viewer.event

import android.content.res.ColorStateList
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.format.DateFormat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.airbnb.epoxy.SimpleEpoxyController
import com.eventvods.api.models.Content
import com.eventvods.api.models.MatchInfo
import com.eventvods.api.models.Matches
import com.eventvods.api.models.Module
import com.eventvods.viewer.*
import com.eventvods.viewer.utils.KotlinEpoxyModel
import com.eventvods.viewer.youtube.YouTubeWebviewPlayerActivity
import com.github.ajalt.timberkt.Timber
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_event_day.view.*
import kotlinx.android.synthetic.main.item_event_game.view.*
import kotlinx.android.synthetic.main.item_event_matches.view.*
import kotlinx.android.synthetic.main.view_event.*
import kotlinx.android.synthetic.main.view_event.view.*
import org.threeten.bp.format.DateTimeFormatter
import org.threeten.bp.temporal.ChronoUnit
import java.util.*


class EventView : androidx.fragment.app.Fragment(), BaseView<EventViewModel> {
    companion object {
        private const val ARG_EVENT_ID = "event_id"
        private const val ARG_GAME_ID = "game_id"

        fun newInstance(eventId: String, gameId: String) = EventView().apply {
            arguments = Bundle().apply {
                putString(ARG_EVENT_ID, eventId)
                putString(ARG_GAME_ID, gameId)
            }
        }
    }

    private lateinit var eventId: String
    private val items = SimpleEpoxyController()


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.view_event, container, false)

    private lateinit var itemBackgroundDrawable: ColorDrawable

    private lateinit var gameId: String

    lateinit var viewModel: EventViewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        eventId = arguments?.getString(ARG_EVENT_ID) ?: ""
        gameId = arguments?.getString(ARG_GAME_ID) ?: ""

        viewModel = ViewModelProviders.of(this).get(EventViewModel::class.java)

        if (eventId.isBlank()) {
            Toast.makeText(context, getString(R.string.event_blankReference), Toast.LENGTH_LONG).show()
            Di.instance.router.exit()
        }

        bindToViewModel(viewModel)

        val gameId = gameId
        itemBackgroundDrawable = ColorDrawable(
            activity!!.getColorCompat(
                secondaryGameColors[gameId]
                    ?: R.color.semi_transparent_background
            )
        )
        val backgroundColor = activity!!.getColorCompat(
            secondaryGameColors[gameId]
                ?: R.color.backgroundNight
        )

        view.setBackgroundColor(backgroundColor)
        view.event_recyclerView.apply {
            layoutManager = androidx.recyclerview.widget.LinearLayoutManager(activity)
            adapter = items.adapter
        }

        view.event_refreshLayout.setOnRefreshListener { viewModel.load(eventId) }

        view.event_appbar.setBackgroundColor(
            view.context.getColorCompat(
                gameColors[gameId]
                    ?: R.color.semi_transparent_background
            )
        )

        viewModel.load(eventId)
    }

    override fun onResume() {
        super.onResume()
        activity?.let {
            //            it.window.setFlags( WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
//            it.window.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)
            it.window.statusBarColor = it.getColor(
                gameColors[gameId]
                    ?: R.color.backgroundNight
            )
        }
    }

    private fun bindToViewModel(viewModel: EventViewModel) {

        viewModel.observableState.observe(this, Observer<EventViewModel.EventState> {
            render(it!!)
        })
    }

    private fun render(eventState: EventViewModel.EventState) {
        event_refreshLayout.isRefreshing = eventState.isLoading

        eventState.event?.let { event ->
            Picasso.get()
                .load(event.logoUri.ifBlank { null })
                .placeholder(R.drawable.icon_lol)
                .into(event_image)
            event_appbar.title = event.name
            event_appbar.subtitle = event.subtitle
//            event_name.text = event.name
//            event_subtitle.text = event.subtitle

            try {
                val days = event.contents
                    ?.mapNotNull { it.modules }
                    ?.flatten()
                    ?.groupBy {
                        it.date?.truncatedTo(ChronoUnit.DAYS)
                    }
                val newItems = mutableListOf<KotlinEpoxyModel>()
                event.contents?.forEach { content ->
                    //                    newItems += ContentItem(content)
                    content.modules?.forEach { module ->
                        newItems += DayItem(content, module)
                        module.matches?.forEach { matches ->
                            newItems += MatchesItem(matches)
                            matches.matchInfos?.forEachIndexed { index, matchInfo ->
                                newItems += MatchItem(index, matchInfo)
                            }
                        }
                    }
                }


                items.setModels(newItems)
            } catch (e: Exception) {
                Timber.e(e)
            }

        }

//        if (eventState.error == null) {
//            visibility = View.GONE
//        } else {
//            text = eventState.error
//            visibility = View.VISIBLE
//        }

    }

    private inner class DayItem(val content: Content, val module: Module) :
        KotlinEpoxyModel(R.layout.item_event_day, module.id) {

        override fun bind(view: View) {
            super.bind(view)

            val format = DateFormat.getBestDateTimePattern(Locale.getDefault(), "EdMMM")
            view.item_event_day_dayOfWeek.text = module.date?.format(DateTimeFormatter.ofPattern(format))
//            view.item_event_day_dayOfMonth.text = module.date?.dayOfMonth?.toString() ?: "-"
//            view.item_event_day_monthOfYear.text =
//                module.date?.month?.getDisplayName(TextStyle.SHORT, Locale.getDefault()) + "."
//            view.item_event_day_dayOfWeek.text =
//                module.date?.dayOfWeek?.getDisplayName(TextStyle.SHORT, Locale.getDefault())?.toString() ?: "-"

            view.item_event_day_eventSection.text = content.title

            view.background.setTint(
                view.context.getColorCompat(
                    secondaryGameColors[gameId]
                        ?: R.color.semi_transparent_background
                )//.darken(fraction = .05)
            )
        }
    }

    private inner class MatchesItem(val matches: Matches) : KotlinEpoxyModel(R.layout.item_event_matches, matches.id) {

        override fun bind(view: View) {
            super.bind(view)
            view.itemMatches_background.backgroundTintList = ColorStateList.valueOf(
                view.context.getColor(
                    secondaryGameColors[gameId]
                        ?: R.color.semi_transparent_background
                )
            )
            view.itemMatches_descriptor.visibility = View.GONE
            view.itemMatches_team1.text = "${matches.team1?.name}"
            view.itemMatches_team2.text = "${matches.team2?.name}"

            Picasso.get()
                .load(matches.team1?.icon?.ifBlank { null })
                .into(view.itemMatches_team1Logo)

            Picasso.get()
                .load(matches.team2?.icon?.ifBlank { null })
                .into(view.itemMatches_team2Logo)

            view.background.setTint(
                view.context.getColorCompat(
                    secondaryGameColors[gameId]
                        ?: R.color.semi_transparent_background
                )
            )
        }
    }

    private inner class MatchItem(val index: Int, val match: MatchInfo) :
        KotlinEpoxyModel(R.layout.item_event_game, match.id) {

        override fun bind(view: View) {
            super.bind(view)
            view.item_game_name.text = getString(R.string.game, index + 1)
            view.item_game_link_youtube.setOnClickListener {
                activity?.startActivity(
                    YouTubeWebviewPlayerActivity.createIntent(
                        context = activity as FragmentActivity,
                        youtubeUrl = this.match.youtube?.gameStart ?: "",
                        matchId = this.match.id
                    )
                )
            }

            view.background.setTint(
                view.context.getColorCompat(
                    secondaryGameColors[gameId]
                        ?: R.color.semi_transparent_background
                )
            )
        }
    }
}