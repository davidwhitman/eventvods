package com.eventvods.viewer.database

/**
 * Tracks all matches than an user has watched and the amount.
 */
class UserWatchProgressBook(author: Author) : InMemoryDatabaseDecorator<WatchProgress>(DiskBackedDatabase("UserWatchProgress", author))