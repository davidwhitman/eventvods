package com.eventvods.viewer.database

import io.paperdb.Book
import java.util.concurrent.locks.ReentrantLock
import kotlin.concurrent.withLock

/**
 * Wraps a [PaperDb] with a memory cache for better performance.
 */
open class InMemoryDatabaseDecorator<T : Any>(private val paperDb: PaperDb<T>) : PaperDb<T> by paperDb {
    private val inMemoryDb by lazy {
        lock.withLock {
            paperDb.readAllKeys()
                .map { key -> Pair(key, paperDb.read(key)!!) }
                .toMap()
                .toMutableMap()
        }
    }

    /**
     * Use a lock to avoid ever having mismatched data between the memory cache and disk cache.
     */
    private val lock = ReentrantLock()

    override fun destroy() {
        lock.withLock {
            paperDb.destroy()
            inMemoryDb.clear()
        }
    }

    override fun write(key: String, value: T): Book {
        lock.withLock {
            val newBook = paperDb.write(key, value)
            inMemoryDb[key] = value
            return newBook
        }
    }

    override fun read(key: String): T? {
        lock.withLock {
            return inMemoryDb[key]
        }
    }

    override fun read(key: String, defaultValue: T): T {
        return read(key) ?: defaultValue
    }

    override fun contains(key: String): Boolean {
        lock.withLock {
            return inMemoryDb.containsKey(key)
        }
    }

    override fun delete(key: String) {
        lock.withLock {
            paperDb.delete(key)
            inMemoryDb.remove(key)
        }
    }

    override fun readAllKeys(): List<String> {
        lock.withLock {
            return inMemoryDb.keys.toList()
        }
    }
    override fun readAllKeysAndValues(): List<Pair<String, T>> {
        lock.withLock {
            return inMemoryDb.toList()
        }
    }

    override fun readAll(): List<T> {
        lock.withLock {
            return inMemoryDb.values.toList()
        }
    }

    override fun writeAll(data: List<T>, keyFinder: (T) -> String) {
        lock.withLock {
            data.forEach {
                paperDb.write(keyFinder.invoke(it), it)
                inMemoryDb[keyFinder.invoke(it)] = it
            }
        }
    }
}