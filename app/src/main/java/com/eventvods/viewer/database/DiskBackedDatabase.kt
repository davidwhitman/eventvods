package com.eventvods.viewer.database

import com.github.ajalt.timberkt.Timber
import io.paperdb.Book
import kotlinx.coroutines.channels.ConflatedBroadcastChannel
import kotlinx.coroutines.channels.ReceiveChannel

/**
 * Creates a book, given the title.
 */
typealias Author = (bookTitle: String) -> Book

/**
 * A [Book] (aka document store) that locally stores any given data.
 *
 * @param author creates a book, given the title
 * @param bookTitle what to call the book
 */
open class DiskBackedDatabase<T : Any>(protected val bookTitle: String, author: Author) : PaperDb<T> {
    private val book: Book = author.invoke(bookTitle)
    private val changeNotifier = ConflatedBroadcastChannel<PaperDb.Change>()

    override fun createDataChangedNotifier(): ReceiveChannel<PaperDb.Change> = changeNotifier.openSubscription()

    override fun destroy() {
        try {
            book.destroy()
            changeNotifier.offer(PaperDb.Change.Destroyed("Book $bookTitle destroyed."))
        } catch (ex: Exception) {
            Timber.e(ex)
            throw ex
        }
    }

    override fun write(key: String, value: T): Book {
        return try {
            val newBook = book.write(key, value)
            changeNotifier.offer(
                PaperDb.Change.KeysModified(
                    listOf(key),
                    "Book $bookTitle: Key $key written with value $value."
                )
            )
            newBook
        } catch (ex: Exception) {
            Timber.e(ex)
            throw ex
        }
    }

    override fun read(key: String): T? =
        try {
            book.read(key)
        } catch (ex: Exception) {
            Timber.e(ex)
            throw ex
        }

    override fun read(key: String, defaultValue: T): T =
        try {
            book.read(key, defaultValue)
        } catch (ex: Exception) {
            Timber.e(ex)
            throw ex
        }

    override operator fun contains(key: String): Boolean =
        try {
            book.contains(key)
        } catch (ex: Exception) {
            Timber.e(ex)
            throw ex
        }

    override fun lastModified(key: String): Long =
        try {
            book.lastModified(key)
        } catch (ex: Exception) {
            Timber.e(ex)
            throw ex
        }

    override fun delete(key: String) {
        try {
            book.delete(key)
            changeNotifier.offer(PaperDb.Change.KeysRemoved(listOf(key), "Book $bookTitle: Key $key deleted."))
        } catch (ex: Exception) {
            Timber.e(ex)
            throw ex
        }
    }

    override fun readAllKeys(): List<String> =
        try {
            book.allKeys
        } catch (ex: Exception) {
            Timber.e(ex)
            throw ex
        }

    override fun readAllKeysAndValues(): List<Pair<String, T>> =
        try {
            book.allKeys
                .map { Pair<String, T>(it, book.read(it)) }
        } catch (ex: Exception) {
            Timber.e(ex)
            throw ex
        }

    override fun setLogLevel(level: Int) = book.setLogLevel(level)

    override fun getPath(): String =
        try {
            book.path
        } catch (ex: Exception) {
            Timber.e(ex)
            throw ex
        }

    override fun getPath(key: String): String =
        try {
            book.getPath(key)
        } catch (ex: Exception) {
            Timber.e(ex)
            throw ex
        }

    override fun readAll(): List<T> = book.allKeys.map { book.read<T>(it) }

    override fun writeAll(data: List<T>, keyFinder: (T) -> String) {
        data.forEach { book.write(keyFinder.invoke(it), it) }
        changeNotifier.offer(PaperDb.Change.KeysModified(data.map(keyFinder), ""))
    }
}