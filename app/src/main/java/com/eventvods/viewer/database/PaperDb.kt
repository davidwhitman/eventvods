package com.eventvods.viewer.database

import io.paperdb.Book
import kotlinx.coroutines.channels.ReceiveChannel

/**
 * A [Book] (aka document store) that locally stores any given data.
 */
interface PaperDb<T : Any> {

    /**
     * Emits the key of a changed item whenever data is written to the store.
     */
    fun createDataChangedNotifier(): ReceiveChannel<Change>

    /**
     * Destroys all data saved in Book.
     */
    fun destroy()

    /**
     * Saves any types of POKOs or collections in Book storage.
     *
     * @param key   object key is used as part of object's file name
     * @param value object to save, must have no-arg constructor, can't be null.
     * @param <T>   object type
     * @return this Book instance
     */
    fun write(key: String, value: T): Book

    /**
     * Instantiates saved object using original object class (e.g. LinkedList). Support limited
     * backward and forward compatibility: removed fields are ignored, new fields have their
     * default values.
     *
     * All instantiated objects must have no-arg constructors.
     *
     * @param key object key to read
     * @return the saved object instance or null
     */
    fun read(key: String): T?

    /**
     * Instantiates saved object using original object class (e.g. LinkedList). Support limited
     * backward and forward compatibility: removed fields are ignored, new fields have their
     * default values.
     *
     * All instantiated objects must have no-arg constructors.
     *
     * @param key          object key to read
     * @param defaultValue will be returned if key doesn't exist
     * @return the saved object instance or default value
     */
    fun read(key: String, defaultValue: T): T

    /**
     * Checks if an object with the given key is saved in Book storage.
     *
     * @param key object key
     * @return true if Book storage contains an object with given key, false otherwise
     */
    fun contains(key: String): Boolean

    /**
     * Returns lastModified timestamp of last write in ms.
     * NOTE: only granularity in seconds is guaranteed. Some file systems keep
     * file modification time only in seconds.
     *
     * @param key object key
     * @return timestamp of last write for given key in ms if it exists, otherwise -1
     */
    fun lastModified(key: String): Long

    /**
     * Delete saved object for given key if it exists.
     *
     * @param key object key
     */
    fun delete(key: String)

    /**
     * Returns all keys for objects in book.
     *
     * @return all keys
     */
    fun readAllKeys(): List<String>

    /**
     * Returns all keys and objects in book.
     *
     * @return all keys and objects
     */
    fun readAllKeysAndValues(): List<Pair<String, T>>

    /**
     * Sets log level for internal Kryo serializer
     *
     * @param level one of levels from [com.esotericsoftware.minlog.Log]
     */
    fun setLogLevel(level: Int)

    /**
     * Returns path to a folder containing *.pt files for all keys kept
     * in the current Book. Could be handy for Book export/import purposes.
     * The returned path does not exist if the method has been called prior
     * saving any data in the current Book.
     *
     * See also [.getPath].
     *
     * @return path to a folder locating data files for the current Book
     */
    fun getPath(): String

    /**
     * Returns path to a *.pt file containing saved object for a given key.
     * Could be handy for object export/import purposes.
     * The returned path does not exist if the method has been called prior
     * saving data for the given key.
     *
     * See also `getPath()`.
     *
     * @param key object key
     * @return path to a *.pt file containing saved object for a given key.
     */
    fun getPath(key: String): String

    /**
     * Reads all data from the [Book].
     */
    fun readAll(): List<T>

    /**
     * Writes all data, using the specified function to create a key for each item.
     */
    fun writeAll(data: List<T>, keyFinder: (T) -> String)

    sealed class Change(open val description: String) {
        data class KeysModified(val keysModified: List<String>, override val description: String) : Change(description)
        data class Destroyed(override val description: String) : Change(description)
        data class KeysRemoved(val keysModified: List<String>, override val description: String) : Change(description)
    }
}