package com.eventvods.viewer.database

import com.eventvods.api.models.Event

/**
 * @author David Whitman on 23 May, 2018.
 */
class EventBook(author: Author) : InMemoryDatabaseDecorator<Event>(DiskBackedDatabase("events", author))