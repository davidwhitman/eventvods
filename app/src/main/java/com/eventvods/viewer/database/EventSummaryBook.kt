package com.eventvods.viewer.database

import com.eventvods.api.models.EventSummary

/**
 * @author David Whitman on 20 May, 2018.
 */
class EventSummaryBook(author: Author) :
    InMemoryDatabaseDecorator<EventSummary>(DiskBackedDatabase("eventSummaries", author))