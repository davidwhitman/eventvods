package com.eventvods.viewer.database

/**
 * The amount that an user has watched a specified match, so far, in seconds.
 */
data class WatchProgress(val matchId: String, val secondsWatchedTo: Int)