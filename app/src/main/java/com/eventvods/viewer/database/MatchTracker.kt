package com.eventvods.viewer.database

import com.github.ajalt.timberkt.Timber

/**
 * @author David Whitman on 20 May, 2018.
 */
class MatchTracker(private val userWatchProgressBook: UserWatchProgressBook) {
    fun getIdealStartingSecond(matchId: String, urlEmbeddedStartingSecond: Int): Int {
        return if (!userWatchProgressBook.contains(matchId)) {
            urlEmbeddedStartingSecond
        } else {
            val savedStartingMillis = userWatchProgressBook.read(matchId)!!.secondsWatchedTo

            if (savedStartingMillis > urlEmbeddedStartingSecond)
                savedStartingMillis
            else
                urlEmbeddedStartingSecond
        }
    }

    fun saveWatchProgress(matchId: String, secondsWatched: Int) {
        Timber.v { "Saved watch progress of $secondsWatched seconds for match $matchId." }
        userWatchProgressBook.write(matchId, WatchProgress(matchId, secondsWatched))
    }

    fun getWatchProgress(matchId: String): WatchProgress? = userWatchProgressBook.read(matchId)
}